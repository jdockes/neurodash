# Neurodash

A striped-down version of the NeuroQuery website, as a Voilà dashboard.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Frprimet%2Fneurodash.git/master?urlpath=%2Fvoila%2Frender%2Fminimal_dashboard.py)